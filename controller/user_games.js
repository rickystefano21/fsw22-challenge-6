const { user_games,user_game_bio,user_game_historyplay} = require("../models");
const userfile = require("../method/user.json");
const {v4: uuidv4} = require('uuid');

class user_game_route{
    static async createuser_game(req,res,next){
try {
    const { username,nickname } = req.body;
    await user_games.create({username , nickname});

    res.redirect("/usergame")
} catch (error) {
    console.log(error);
}
    }
    static async edituser_game(req,res,next){
try {
    const { username,nickname } = req.body;
    const id = req.params.id;
    let data = await user_games.update({username,nickname},{
        where :{
            id
        },
        returning : true
    })
res.redirect("/usergame");
    
} catch (error) {
    console.log(error)
}
    }
    
    static async deleteuser_game(req,res,next){
try {
    const id = req.params.id;
    const data = await user_games.destroy({
        where :{
            id
        }
    })
    res.redirect("/usergame")

        } catch (error) {
    console.log(error)
}
    }
    
    static async getuser_game(req,res,next){
        try {
            const data = await user_games.findAll()
            let dataarr = data.map(x=> x.dataValues)
            res.render("home",{data : dataarr});

        } catch (error) {
            console.log(error);
        }
        }
        
    static async getuser_gameid(req,res,next){
        try {
            const id = req.params.id;
            const data = await user_games.findOne({
                where :{
                    id : id
                }
            })
            res.status(200).json(data);
        } catch (error) {
            console.log(error)
        }
    }
    static async edituser_gameview(req,res,next){
        try {
            const id = req.params.id;
            let data = await user_games.findOne({
                where :{
                    id
                },
            })
            data = data.dataValues
            res.render("edit",{data})
        } catch (error) {
            console.log(error)
        }
            }
            
    static async create_view(req,res,next){
                try {
                    res.render("create");
                } catch (error) {
                    
                }
            }
    static async loginpage(req,res,next){
                res.render("login")
              }
    
    static async loginmethod(req,res,next){
                const id = req.body.username;
                const pass = req.body.password;
                let idx = userfile.findIndex(x => x.username === id);
                const checkPassword = userfile[idx].password;
                if(pass == checkPassword){
                  res.redirect("/usergame");
                }else{
                  res.send("eror ngab");
                }
              }


}

module.exports = user_game_route;