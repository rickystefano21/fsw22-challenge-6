const user_game_route = require("./user_games");
const biodatacontrol = require("./user_game_bio");
const historycontroller = require("./user_game_historyplay");


module.exports = {
    user_game_route,
    biodatacontrol,
    historycontroller
};