const { user_game_bio , user_games} = require("../models")

class biodatacontrol {
  static async createbiodata(req, res, next) {
    try {
      const { userGameId,fullname, age , address } = req.body
      const data = await user_game_bio.create({
        userGameId,fullname, age , address
      })
      res.status(201).json(data)
    } catch (error) {
console.log(error)
    }
  }

  static async getbiodata(req, res, next) {
    try {
      const data = await user_game_bio.findAll({
        include: [
          {
            model: user_games
          }
        ]
      })
      let dataarr = data.map(x=> x.dataValues)
      res.render("biodata",{data : dataarr});
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = biodatacontrol;