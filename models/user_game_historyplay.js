'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_historyplay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_historyplay.belongsTo(models.user_games, { foreignKey: 'userGameId' })
    }
  }
  user_game_historyplay.init({
    userGameId: DataTypes.INTEGER,
    win: DataTypes.INTEGER,
    draw: DataTypes.INTEGER,
    lose: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_historyplay',
  });
  return user_game_historyplay;
};