'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_bio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_bio.belongsTo(models.user_games, { foreignKey: 'userGameId' })
    }
  }
  user_game_bio.init({
    userGameId: DataTypes.INTEGER,
    fullname: DataTypes.STRING,
    age: DataTypes.INTEGER,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game_bio',
  });
  return user_game_bio;
};