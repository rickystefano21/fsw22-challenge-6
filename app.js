const express = require("express");
const app = express();
const routes = require("./routes");
const bodyparser = require("body-parser");

app.use(express.static("public"));
app.set("view engine","ejs");   

app.use(express.urlencoded({extended:true}));
app.use(express.json());

app.use(routes);

app.listen(3000, function(){
    console.log("Connected to port 3000");
})